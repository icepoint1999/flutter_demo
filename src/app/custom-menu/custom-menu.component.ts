import {Component, Input, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-custom-menu',
  templateUrl: './custom-menu.component.html',
  styleUrls: ['./custom-menu.component.scss']
})
export class CustomMenuComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  @Input() menus = [];

  cities = [
    {name: 2},
    {name: 3},
    {name: 1},
  ];

  templateDrop(event: CdkDragDrop<string[]>, index?: number): void {
    moveItemInArray(this.cities, event.previousIndex, event.currentIndex);
    // this.cities = [...this.cities];
  }
}
