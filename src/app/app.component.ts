import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ant-demos';

  menus = [
    {
      name: 'asd3233'
    },
    {
      name: 'asd'
    },
    {
      name: 'has children',
      children:[
        {name:'xxx'},
        {name:'xxx'},
        {name:'xxx'}
      ]
    },

    {
      name:'asd'
    },
    {
      name:'asd'
    },
    {
      name:'asd'
    },


  ];
}
