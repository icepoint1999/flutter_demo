import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {CustomMenuComponent} from './custom-menu/custom-menu.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {AtModule} from 'at-ng';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgZorroAntdModule, AtModule],
      declarations: [
        AppComponent,
        CustomMenuComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ant-demos'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('ant-demos');
  });
});
